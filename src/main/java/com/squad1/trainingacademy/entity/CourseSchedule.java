package com.squad1.trainingacademy.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CourseSchedule {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long courseScheduleId;
	@ManyToOne
	private Course course;
	private String trainer;
	private LocalDate startDate;
	private LocalDate endDate;
}
