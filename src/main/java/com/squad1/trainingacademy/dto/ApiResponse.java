package com.squad1.trainingacademy.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message,String httpStatus) {

}
