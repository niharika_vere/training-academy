package com.squad1.trainingacademy.util;

public interface SuccessResponse {
	
	String SUCCESS_CODE1 = "SUCCESS01";
	String SUCCESS_MESSAGE1 = "Training plan fetched successfully";
	
	String SUCCESS_CODE2 = "SUCCESS02";
	String SUCCESS_MESSAGE2="Enrolled for the Course";

	String SUCCESS_CODE3 = "SUCCESS03";
	String SUCCESS_MESSAGE3 = "Enrolled course updated";
}
