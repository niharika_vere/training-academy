package com.squad1.trainingacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.trainingacademy.entity.CourseSchedule;

public interface CourseScheduleRepository extends JpaRepository<CourseSchedule, Long>{

}
