package com.squad1.trainingacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.trainingacademy.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
