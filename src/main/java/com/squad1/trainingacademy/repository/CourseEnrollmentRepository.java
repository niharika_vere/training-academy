package com.squad1.trainingacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.trainingacademy.entity.CourseEnrollment;

public interface CourseEnrollmentRepository extends JpaRepository<CourseEnrollment, Long>{

}
