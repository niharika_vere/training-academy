package com.squad1.trainingacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.trainingacademy.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Long>{

}
